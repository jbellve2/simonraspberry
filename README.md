# Exercicis per realitzar el joc de memory de Simon en una raspberry pi, 4 leds i Python


## 1) Crear una carpeta amb el vostre cognom_Nom al /home/pi de la raspberry
```console
mkdir cognom_Nom
```

## 2) Crear el programa simon01.py amb aquesta plantilla:

```python
# Joc de simon en 4 leds
# ('')--,--,-


import RPi.GPIO as GPIO
import time
import random

# enxufarem un led al pins 11,13,15 i 19
# els leds han de ser d'aquests colors:
# 11 -> roig.  13 -> verd.  15 -> blau.  19 -> groc
# recorda que els leds ha d'anar a un resistor de 200k (almenys) 
# per la part més curta del led i l'altra part al pin escollit (11,13,15,19)
# la part més llarga del led anirà a un cable directe a un gnd 
# (per exemple el pin 6, 14, 20, etc...)
#
# utilitzem sistema de numeració BOARD
GPIO.setmode (GPIO.BOARD)
GPIO.setwarnings(False) # ignorem els warnings



temps=1                 # per si volem fer-ho més ràpid a cada vegada
ronda=1                 # contador de rondes fetes correctament
# VARIABLE leds (exercici simon.01.py)      # vector amb el conjunt de leds
final=False             # per indicar quan el programa acaba (quan fallem serà final=True)

try:

## resta de codi


##### ... ací és on has de posar el teu codi  #####


## resta de codi

except KeyboardInterrupt:
	GPIO.cleanup()


###### final plantilla ######

```


## 3) Continuar el programa `simon01.py`. Crea el vector amb els pins 11,13,15,19

```
leds = [11,13,15,19]
```

Crea una variable que tinga com a valor aleatori un d'aquests pins. Per fer-ho podem utilitzar aquest mètode: random.sample

Fes també que s'ilumine un led aleatori (1 segon) i després s'apague (i que pare 0,5 segons).

```python
# surt un led random dels del vector: leds                   
pinLED=random.sample(leds,1)[0]                               
GPIO.setup(pinLED,GPIO.OUT)   # per indicar que el pin és de sortida        
```

El mètode **random.sample()** fa que dintre del vector leds, ens retorne 1 valor. El [0] és per que ens el retorne en format String i no en format llista 

## 4) Continuar el programa `simon02.py`.Fes un bucle que faci el que hem fet abans 4 vegades. I que guarde en una variable String el caràcter corresponent a cada color:

- Si apareix el led-  11 (roig) guardarà una 'r'
- Si apareix el led 13 (verd) guardarà una 'v'
- Si apareix el led 15 (blau) guardarà una 'b'
- Si apareix el led 19 (groc) guardarà una 'g'

I imprimeixo per pantalla
Per fer-ho copia el programa anterior i canvia-li el nom:

```console
cp simon01.py simon02.py
nano simon02.py
```

Mini plantilla per fer l'exercici

```python

               colorsPC=""				       # cadena per afegir  caràcters

		# <codi>inici bucle while (4 cops) 
			
                        if (<codi>):                            # si és el pin 11 (roig)
                                <codi>                          # afegim a la cadena una "r"
                        elif (<codi>)                           # si és el pin 13 (verd)
                                <codi>           		# afegim a la cadena una "v"
                        elif (<codi>)                     	# si és el pin 15 (blau)
                                <codi>           		# afegim a la cadena una "b"
                        elif (<codi>):                          # si és el pin 19 (groc)
                                <codi> 		                # afegim a la cadena una "g"
		
		# <codi>control codi while

		# final bucle while
```

## 5) Fer el programa `simon03.py` a partir de l’anterior.
```console
cp simon02.py simon03.py
nano simon03.py
```

Al final del bucle de 4 vegades, pregunta a l'usuari pels colors que han sortit. Ha de posar els colors en minúscula i sense cap separació. Per exemple si ha sortit: roig, roig, verd, blau, l'usuari haurà de posar: rrvb

- Si el que ha sortit és el mateix que el que ha posat apareixerà un missatge amb felicitacions i tornarà a crear 4 leds més. 
- Si el que ha sortit no és el mateix que el que ha posat, apareixerà un missatge dient que ha fallat i acaba el joc.

Sols has d'agafar el que et diu l'usuari i comparar-la amb la variable generada abans. La dels colors creats aleatòriament pel programa. Es pot fer amb un simple if.

## 6) Fer el programa `simon04.py` a partir de l’anterior. (Mira plantilla següent)

```console
cp simon03.py simon04.py
nano simon04.py
```

Ja sols queda que hi hagi vàries rondes. En la primera ronda hi haurà 2 leds, en la segona 3, en la 3a 4 leds, etc... Passarem de ronda sempre que l'usuari encerte tots els leds en el mateix ordre. Quan l'usuari falle, ens mostrarà els punts que hem aconseguit (número de ronda), els leds que han sortit i els leds que ha posat l'usuari 


## Plantilla per fer `simon04.py`

```python

# ULL EN AQUESTA PLANTILLA DE CODI HI HAN ERRORS QUE HAURIEU DE SABER CORREGIR!!



final=False             # per indicar quan el programa acaba (quan fallem serà final=True)
ronda=1
try:
        while (final==False)					# controlem el final del joc
                print("")                                       # salt de línia
                print("preparat??!!")                           # anunciem la ronda
                time.sleep(3)                                   # parem 3 segons
                 <codi>                           		# incrementem ronda en 1
                cont=ronda                                      # número de leds dintre d'una ronda
                colorsPC=""                                     # colors que sortiran aleatoriament
                colorsUsuari=""                                 # colors que introduirà l'usuari
                while (cont>0)
                        <codi>                                  # surt un led random del vector
                        <codi>                                  # per dir que el pin és de sortida

                        <codi>                                  # engegem el led1 segon
                        <codi> 	                                # parem 1 segon. 

                        <codi>                   		# apaguem el led
                        <codi> 	                                # esperem 0.5 segons 

                        if (<codi>):                            # si és el pin 11 (roig)
                                <codi>                          # afegim a la cadena una "r"
                        elif (<codi>)                           # si és el pin 13 (verd)
                                <codi>           		# afegim a la cadena una "v"
                        elif (<codi>)                     	# si és el pin 15 (blau)
                                <codi>           		# afegim a la cadena una "b"
                        elif (<codi>):                          # si és el pin 19 (groc)
                                <codi> 		                # afegim a la cadena una "g"

                             <codi>                             # decrementem el comptador de leds


                # demanem a l'usuari que ens done els colors
                <codi>

                # si els colors són els mateixos dels que han sortit:
                if (<codi>):
                        print("Correcte")		# informem que ha encertat els colors
                # sinò ho son:
                else:
                        print("INCORRECTE!")
                        <codi>	# informem que l'usuari ha posat xxx i ha sortit yyy
                        <codi>	# informem dels punts aconseguits
                        <codi>	# tanquem el bucle

                time.sleep(1)        # per parar un poc després d'encertar o fallar

except KeyboardInterrupt: GPIO.cleanup()


```



# ESQUEMA CONNEXIÓ LED


- El cable roig en aquest cas va connectat al pin11 (led roig)
- El cable verd en aquest cas va connectat al pin13 (led verd)
- El cable blau en aquest cas va connectat al pin15 (led blau)
- El cable groc en aquest cas va connectat al pin19 (led groc)
- El cable negre ground (amb la resistència al pin 6, (GND)


![Image text](https://gitlab.com/jbellve2/simonraspberry/-/raw/master/imatges/simonLed.png?ref_type=heads)




