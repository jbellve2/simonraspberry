# Import the GPIO and time libraries
# Joc de simon en 4 leds
# Josep Vicent Bellver
# ('')--,--,-
# 2023-11-18

#Fes un bucle que faci el que hem fet abans 4 vegades. I que guarde en una variable String això:
#Si apareix el led 11 (roig) guardarà una 'r'
#Si apareix el led 13 (verd) guardarà una 'v'
#Si apareix el led 15 (blau) guardarà una 'b'
#Si apareix el led 19 (groc) guardarà una 'g'

#I imprimeixo per pantalla


import RPi.GPIO as GPIO
import time
import random
# enxufarem un led al pins 11,13,15 i 19
# els leds han de ser d'aquests colors:
# 11 -> roig.  13 -> verd.  15 -> blau.  19 -> groc
# recorda que els leds ha d'anar a un resistor de 200k (almenys) per la part més curta del led i l'altra part al pin escollit (11,13,15,19)
# la part més llarga del led anirà a un cable directe a un gnd (per exemple el pin 6, 14, 20, etc...)

# utilitzem sistema de numeració BOARD
GPIO.setmode (GPIO.BOARD)
GPIO.setwarnings(False)	# ignorem els warnings


temps=1			# per si volem fer-ho més ràpid a cada vegada
ronda=1			# contador de rondes fetes correctament
leds=[11,13,15,19]	# vector amb el conjunt de leds
final=False		# per indicar quan el programa acaba (quan fallem serà final=True)

try:
	cont=4
	colorPC=""
	while (cont>0):

		pinLED=random.sample(leds,1)[0]		# surt un led random dels del vector: leds
		GPIO.setup(pinLED,GPIO.OUT)		# posem el gpio escollit com a sortida (li entrarà llum al led)

		GPIO.output(pinLED,1)			# engegem el led un temps (per defecte 1 segon).
		time.sleep(temps)			# parem eixe temps. La variable temps la podem decrementar cada ronda fent més ràpida aquesta

		GPIO.output(pinLED,0)			# apaguem el led
		time.sleep(0.5)				# esperem un temps abans d'engegar l'altre led

		if (pinLED==11):
			colorPC=colorPC+"r"
		elif (pinLED==13):
			colorPC=colorPC+"v"
		elif (pinLED==15):
			colorPC=colorPC+"b"
		elif (pinLED==19):
			colorPC=colorPC+"g"
		cont=cont-1

	print("colorsGenerats: ",colorPC)

except KeyboardInterrupt: GPIO.cleanup()




