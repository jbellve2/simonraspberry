# Import the GPIO and time libraries
# Joc de simon en 4 leds
# Josep Vicent Bellver
# ('')--,--,-
# 2023-11-18


# simon03.py
# al final del bucle de 4 vegades, pregunta a l'usuari pels colors que han sortit.
# Ha de posar els colors en minúscula i sense cap separació.
# Per exemple si ha sortit: roig, roig, verd, blau, l'usuari haurà de posar: rrvb

# Si el que ha sortit és el mateix que el que ha posat apareixerà un missatge amb felicitacions i tornarà a crear 4 leds més.
# Si el que ha sortit no és el mateix que el que ha posat, apareixerà un missatge dient que ha fallat i acaba el joc.





import RPi.GPIO as GPIO
import time
import random
# enxufarem un led al pins 11,13,15 i 19
# els leds han de ser d'aquests colors:
# 11 -> roig.  13 -> verd.  15 -> blau.  19 -> groc
# recorda que els leds ha d'anar a un resistor de 200k (almenys) per la part més curta del led i l'altra part al pin escollit (11,13,15,19)
# la part més llarga del led anirà a un cable directe a un gnd (per exemple el pin 6, 14, 20, etc...)

# utilitzem sistema de numeració BOARD
GPIO.setmode (GPIO.BOARD)
GPIO.setwarnings(False)	# ignorem els warnings


temps=1			# per si volem fer-ho més ràpid a cada vegada
ronda=1			# contador de rondes fetes correctament
leds=[11,13,15,19]	# vector amb el conjunt de leds
final=False		# per indicar quan el programa acaba (quan fallem serà final=True)

try:
	while (final==False):
		cont=4
		colorsPC=""
		while (cont>0):
			pinLED=random.sample(leds,1)[0]		# surt un led random dels del vector: leds
			GPIO.setup(pinLED,GPIO.OUT)		# posem el gpio escollit com a sortida (li entrarà llum al led)

			GPIO.output(pinLED,1)			# engegem el led un temps (per defecte 1 segon).
			time.sleep(temps)			# parem eixe temps. La variable temps la podem decrementar cada ronda fent més ràpida aquesta

			GPIO.output(pinLED,0)			# apaguem el led
			time.sleep(0.5)				# esperem un temps abans d'engegar l'altre led

			if (pinLED==11):
				colorsPC=colorsPC+"r"
			elif (pinLED==13):
				colorsPC=colorsPC+"v"
			elif (pinLED==15):
				colorsPC=colorsPC+"b"
			elif (pinLED==19):
				colorsPC=colorsPC+"g"
			cont=cont-1

		#print("colorsGenerats: ",colorPC)
		colorsUsuari=input("Dis-me els colors que han sortit sense comes. Exemple rggvb: ")
		if (colorsUsuari==colorsPC):
			print("Correcte")
		else:
			print("INCORRECTE!")
			final=True

		time.sleep(1)

except KeyboardInterrupt: GPIO.cleanup()




