# Import the GPIO and time libraries
# Joc de simon en 4 leds
# Josep Vicent Bellver
# ('')--,--,-
# 2023-11-18


# simon04.py
# Sols queda que hi hagi vàries rondes.
# En la primera ronda hi haurà 2 leds, en la segona 3, en la 3a 4 leds, etc...
# Passarem de ronda sempre que l'usuari encerte tots els leds en el mateix ordre.
# Quan l'usuari falle, ens mostrarà els punts que hem aconseguit (número de ronda),
# els leds que han sortit i els leds que ha posat l'usuari
# amb un missatge així: Havia sortit:  vbbr  i tu has posat:  vbrb



import RPi.GPIO as GPIO
import time
import random
# enxufarem un led al pins 11,13,15 i 19
# els leds han de ser d'aquests colors:
# 11 -> roig.  13 -> verd.  15 -> blau.  19 -> groc
# recorda que els leds ha d'anar a un resistor de 200k (almenys) per la part més curta del led i l'altra part al pin escollit (11,13,15,19)
# la part més llarga del led anirà a un cable directe a un gnd (per exemple el pin 6, 14, 20, etc...)

# utilitzem sistema de numeració BOARD
GPIO.setmode (GPIO.BOARD)
GPIO.setwarnings(False)	# ignorem els warnings


temps=1			# per si volem fer-ho més ràpid a cada vegada
ronda=1			# contador de rondes fetes correctament
leds=[11,13,15,19]	# vector amb el conjunt de leds
final=False		# per indicar quan el programa acaba (quan fallem serà final=True)
ronda=1
try:
	while (final==False):
		print("")					# salt de línia
		print("preparat??!!")				# anunciem la ronda
		time.sleep(3)					# parem 3 segons
		ronda=ronda+1					# comptador de ronda
		cont=ronda					# comptador de leds dintre d'una ronda
		colorsPC=""					# colors que sortiran aleatoriament
		colorsUsuari=""					# colors que introduirà l'usuari
		while (cont>0):
			pinLED=random.sample(leds,1)[0]		# surt un led random dels del vector: leds
			GPIO.setup(pinLED,GPIO.OUT)		# posem el gpio escollit com a sortida (li entrarà llum al led)

			GPIO.output(pinLED,1)			# engegem el led un temps (per defecte 1 segon).
			time.sleep(temps)			# parem eixe temps. La variable temps la podem decrementar cada ronda fent més ràpida aquesta

			GPIO.output(pinLED,0)			# apaguem el led
			time.sleep(0.5)				# esperem un temps abans d'engegar l'altre led

			if (pinLED==11):			# pin roig
				colorsPC=colorsPC+"r"
			elif (pinLED==13):
				colorsPC=colorsPC+"v"		# pin verd
			elif (pinLED==15):
				colorsPC=colorsPC+"b"		# pin blau
			elif (pinLED==19):
				colorsPC=colorsPC+"g"		# pin groc
			cont=cont-1				# comptador de leds dintre d'una ronda


		# demanem a l'usuari que ens done els colors
		colorsUsuari=input("Dis-me els colors que han sortit sense comes. Exemple rggvb: ")

		# si els colors són els mateixos dels que han sortit:
		if (colorsUsuari==colorsPC):
			print("Correcte")
		# sinò ho son:
		else:
			print("INCORRECTE!")
			print("Havia sortit: ",colorsPC," i tu has posat: ",colorsUsuari)
			print("Has aconseguit ",str(ronda-2)," punts")				# ronda -2 ja que l'última no es conta
			final=True

		time.sleep(1)						# per parar un poc després d'encertar o fallar

except KeyboardInterrupt: GPIO.cleanup()




