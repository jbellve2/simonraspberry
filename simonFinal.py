# Import the GPIO and time libraries
# Joc de simon en 4 leds
# Josep Vicent Bellver
# ('')--,--,-
# 2023-11-18
import RPi.GPIO as GPIO
import time
import random
# enxufarem un led al pins 11,13,15 i 19
# els leds han de ser d'aquests colors:
# 11 -> roig.  13 -> verd.  15 -> blau.  19 -> groc
# recorda que els leds ha d'anar a un resistor de 200k (almenys) per la part més curta del led i l'altra part al pin escollit (11,13,15,19)
# la part més llarga del led anirà a un cable directe a un gnd (per exemple el pin 6, 14, 20, etc...)

# utilitzem sistema de numeració BOARD
GPIO.setmode (GPIO.BOARD)
GPIO.setwarnings(False)	# ignorem els warnings



#pinLED=15		# LED CONNECTAT AL PIN 15

temps=1			# per si volem fer-ho més ràpid a cada vegada
ronda=1			# contador de rondes fetes correctament
leds=[11,13,15,19]	# vector amb el conjunt de leds
final=False		# per indicar quan el programa acaba (quan fallem serà final=True)

try:

	while final==False:		# final del joc (en cas que fallem, final=True i sortirem d'aquest bucle
		ronda=ronda+1		# contador de rondes
		qLeds=ronda		# quantitat de leds que engegarem
		llistaColors=["r","b"]	# creem una llista de colors. La inicie per definir una llista i després la buide
		llistaColors.clear()	# buidem la llista. Almenys s'ha creat una llista
		print("\npreparat...\n") # avisem
		time.sleep(3)		# parem 3 segons a l'inci de cada ronda

		while qLeds>0:


			pinLED=random.sample(leds,1)[0]		# surt un led random dels del vector: leds

			GPIO.setup(pinLED,GPIO.OUT)		# posem el gpio escollit com a sortida (li entrarà llum al led)

			GPIO.output(pinLED,1)			# engegem el led un temps (per defecte 1 segon).
			time.sleep(temps)			# parem eixe temps. La variable temps la podem decrementar cada ronda fent més ràpida aquesta

			GPIO.output(pinLED,0)			# apaguem el led
			time.sleep(0.5)				# esperem un temps abans d'engegar l'altre led

			qLeds=qLeds-1				# restem un led menys per ronda (cada ronda hi haurà un led més)

			# fem la llista de colors a partir del pin elegit:
			# codificació:
			# pin	color
			# 11	r -> roig
			# 13	v -> verd
			# 15	b -> blau
			# 19	g -> groc
			if ( pinLED == 11 ):	# roig
				llistaColors.append("r")	# afegim una "r"
			if ( pinLED == 13 ):	# verd
				llistaColors.append("v")	# afegim una "v"
			if ( pinLED == 15 ):	# blau
				llistaColors.append("b")	# afegim una "b"
			if (pinLED==19):	# groc
				llistaColors.append("g")	# afegim una "g"

		colors=input('Dis-me els colors (r,v,b,g): (sense comes)')	# serà una variable String
		colorsPC = ''.join(llistaColors)		# passem la llista a variable String

		# comparem si el jugador ho ha fet correcte
		if (colors==colorsPC):
			time.sleep(0.5)
			print("perfecte!")

			# ILUMINEM ELS LEDS SEQÜENCIALMENT PER DIR QUE HEM ENCERTAT
			# posem tots els leds en mode out
			GPIO.setup(11,GPIO.OUT)
			GPIO.setup(13,GPIO.OUT)
			GPIO.setup(15,GPIO.OUT)
			GPIO.setup(19,GPIO.OUT)

			# engeguem tots amb una pausa petita entre ells
			GPIO.output(11,1)
			time.sleep(0.2)

			GPIO.output(13,1)
			time.sleep(0.2)

			GPIO.output(15,1)
			time.sleep(0.2)

			GPIO.output(19,1)
			time.sleep(0.2)

			# apaguem tots
			GPIO.output(11,0)
			GPIO.output(13,0)
			GPIO.output(15,0)
			GPIO.output(19,0)


		# o si no ho ha fet correcte
		else:
			print("fail")

			error=5
			GPIO.setup(11,GPIO.OUT)
			# ENGEGUEM EL LED VERMELL 5 COPS
			while (error > 0 ):
				GPIO.output(11,1)	# engeguem
				time.sleep(0.1)
				GPIO.output(11,0)	# tanquem
				time.sleep(0.1)

				error=error-1

			print("Havia sortit: ",colorsPC," i tu has posat", colors)
			time.sleep(1)
			final=True
			break;

	print("has aconseguit ",ronda," punts")

except KeyboardInterrupt:
    GPIO.cleanup()




